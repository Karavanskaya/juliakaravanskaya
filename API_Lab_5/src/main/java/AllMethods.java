import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class AllMethods {

    public ArrayList<String> urls;
    public ArrayList<String> expected;
    public ArrayList<String> response;
    public ArrayList<String> unit;

    public AllMethods() {
        urls = new ArrayList<String>();
        expected = new ArrayList<String>();
        response = new ArrayList<String>();
        unit = new ArrayList<String>();

    }


    public void readFile() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("Myfile.txt"));
        String[] tmpmas = {"",""};

        String tmp = "";

        while ((tmp = br.readLine()) != null) {


            tmpmas = tmp.split("   ");
            urls.add(tmpmas[0]);
            expected.add(tmpmas[1]);

        }

        br.close();

    }

    public void getResponse() throws IOException {

        for (int i = 0; i < urls.size(); i++) {
            String tmpResponse = "";
            URL url = new URL(urls.get(i));
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");


            if (conn.getResponseCode() != 200)
            {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br1 = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            String output;
            while ((output = br1.readLine()) != null)
            {
                tmpResponse += output;


            }

            conn.disconnect();
            response.add(tmpResponse);

        }


    }




    public void fillUnitsArray() throws ParserConfigurationException, IOException, SAXException, XPathExpressionException
    {

        for(int i=0;i<response.size();i++)

        {
            InputSource source = new InputSource(new StringReader(response.get(i)));


            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = db.parse(source);

            XPathFactory xpathFactory = XPathFactory.newInstance();
            XPath xpath = xpathFactory.newXPath();

            unit.add(xpath.evaluate("string(.//time[1]//temperature/@unit)", document));



        }

    }

    public boolean compare()
    {
        boolean result = true;

        for(int i = 0;i<urls.size();i++)
        {

           if(!unit.get(i).equals(expected.get(i)))
           {
               result = false;
           }
        }
        return result;

    }

    public void addResultToFile() throws IOException {
        BufferedWriter out = new BufferedWriter(new FileWriter("Myfile.txt"));
        for (int i = 0; i < urls.size(); i++)
        {
            out.write(urls.get(i) + "   " + expected.get(i) + "   " + unit.get(i).equals(expected.get(i)) + "\r\n");
        }
        out.close();

    }



}

