import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;

public class MainTest
{

    @Test
    public void test() throws IOException, ClassNotFoundException, ParserConfigurationException, InstantiationException, SAXException, IllegalAccessException, XPathExpressionException {

    AllMethods all = new AllMethods();

        all.readFile();
        all.getResponse();
        all.fillUnitsArray();
        all.addResultToFile();
        Assert.assertTrue("Not all data is the same as expected.", all.compare());


}
}