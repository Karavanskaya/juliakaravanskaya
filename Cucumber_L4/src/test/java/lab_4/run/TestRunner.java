package lab_4.run;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.concurrent.TimeUnit;

/**
 * Created by ASUS on 08.06.2016.
 */

@RunWith(Cucumber.class)

        @CucumberOptions(
        features = "src/test/java/lab_4/features",
        glue = "lab_4/steps",
        tags = "@testrozetka"      //4)чтобы запуститить только тесты розетки
        //tags="@two"                //2)чтобы запуститить по 2 теста для розетки и 2 для стековерфлоу
        //tags = "@testrozetka, @teststackoverflow"        //3)чтобы запуститить все тесты
        //tags = "@teststackoverflow"  //4)чтобы запуститить только тесты стековерфлоу
)
public class TestRunner {
    public static WebDriver driver;

    @BeforeClass
    public static void setUp() {
        driver = new FirefoxDriver();
    }

    @AfterClass
    public static void tearDown(){
        driver.quit();
    }
}
