package lab_4.steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lab_4.run.TestRunner;
import org.openqa.selenium.By;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;
/**
 * Created by ASUS on 08.06.2016.
 */
public class MyStepdefsRoz {
    @Given("^I on rozetka start page$")
    public void iOnRozetkaStartPage() throws Throwable {
        assertTrue("Log is not displayed",
                TestRunner.driver.findElement(By.xpath(".//img[contains(@alt, 'Интернет магазин Rozetka.ua™ - №1')]")).isDisplayed());

    }

    @Then("^I see page with rozetka logo$")
    public void iSeePageWithRozetkaLogo() throws Throwable {
        assertTrue("Log is not displayed",
                TestRunner.driver.findElement(By.xpath(".//img[contains(@alt, 'Интернет магазин Rozetka.ua™ - №1')]")).isDisplayed());

    }

    @Then("^I see page with Apple field$")
    public void iSeePageWithAppleField() throws Throwable {
        assertTrue("'Apple' is not displayed",
                TestRunner.driver.findElement(By.xpath("//ul/li/a[contains(@href,'apple')]")).isDisplayed());
        throw new PendingException();
    }

    @Then("^I see page with Telephony,mp(\\d+) field$")
    public void iSeePageWithTelephonyMpField(int arg0) throws Throwable {
        assertTrue("'mp3' is not displayed",
                TestRunner.driver.findElement(By.xpath("//ul/li/a[contains(@href,'mp3')]")).isDisplayed());

    }

    @When("^I click link City$")
    public void iClickLinkCity() throws Throwable {
        TestRunner.driver.findElement(By.xpath(".//*[@id='city-chooser']/a")).click();

    }

    @Then("^I see page with Odessa, Kharkov, Kiev cities$")
    public void iSeePageWithOdessaKharkovKievCities() throws Throwable {
        assertTrue("'Одесса' is not displayed",
                TestRunner.driver.findElement(By.xpath(".//div[@class='header-city-box clearfix']//a[contains(text(),'Одесса')]")).isDisplayed());
        assertTrue("'Харьков' is not displayed",
                TestRunner.driver.findElement(By.xpath(".//*[@id='city-chooser']/descendant::*[@locality_id='31']")).isDisplayed());
        assertTrue("'Киев' is not displayed",
                TestRunner.driver.findElement(By.xpath(".//div[@class='header-city-box clearfix']//a[contains(text(),'Киев')]")).isDisplayed());


    }

    @When("^I click link Kart$")
    public void iClickLinkKart() throws Throwable {
        TestRunner.driver.findElement(By.xpath(".//div/*/a[contains(@href,'cart')]")).click();

    }

    @Then("^I see page with kart is empty$")
    public void iSeePageWithKartIsEmpty() throws Throwable {
        assertTrue("'Корзина пуста' is not displayed",
                TestRunner.driver.findElement(By.xpath(".//div/h2[text()='Корзина пуста']")).isDisplayed());

    }

    @Given("^the user is on main page$")
    public void theUserIsOnMainPage() throws Throwable {
        TestRunner.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (!TestRunner.driver.getCurrentUrl().equals("http://rozetka.com.ua/"))
            TestRunner.driver.get("http://rozetka.com.ua/");
        TestRunner.driver.manage().window().maximize();
    }

}
