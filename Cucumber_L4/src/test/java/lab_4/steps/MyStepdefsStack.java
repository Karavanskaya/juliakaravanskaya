package lab_4.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lab_4.run.TestRunner;
import org.openqa.selenium.By;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created by ASUS on 09.06.2016.
 */
public class MyStepdefsStack {
    @Given("^I on stackoverflow start page$")
    public void iOnStackoverflowStartPage() throws Throwable {
        assertTrue("Log is not displayed",
                TestRunner.driver.findElement(By.xpath(".//*[@id='hlogo']/a")).isDisplayed());
    }

    @Then("^I see page with featured count more then (\\d+)$")
    public void iSeePageWithFeaturedCountMoreThen(int arg0) throws Throwable {
        assertTrue("Количество в табе ‘featured' меньше 300",
                Integer.parseInt(TestRunner.driver.findElement(By.xpath(".//div[@id='tabs']/*/span[contains(@class,'bounty-indicator-tab')]")).getText()) > 300);

    }

    @When("^I click link Sign up button$")
    public void iClickLinkSignUpButton() throws Throwable {
        TestRunner.driver.findElement(By.xpath(".//*[@id='tell-me-more']")).click();

    }

    @Then("^I see page with Google and Facebook button$")
    public void iSeePageWithGoogleAndFacebookButton() throws Throwable {
        assertTrue("'Google' is not displayed",
                TestRunner.driver.findElement(By.xpath(".//div/*/span[contains(text(),'Google')]")).isDisplayed());
        assertTrue("'Facebook' is not displayed",
                TestRunner.driver.findElement(By.xpath(".//div/*/span[contains(text(),'Facebook')]")).isDisplayed());

    }

    @When("^I click on any Top Question$")
    public void iClickOnAnyTopQuestion() throws Throwable {
        TestRunner.driver.findElement(By.xpath(".//div[@class='summary']/*/a[contains(text(),'How')]")).click();

    }

    @Then("^I see page with today date$")
    public void iSeePageWithTodayDate() throws Throwable {
        assertTrue("Can't find element",
                TestRunner.driver.findElement(By.xpath(".//*[@id='qinfo']//tbody/tr/td/p/b[contains(text(),'today')]")).isDisplayed());

    }

    @Given("^the user is on main page of Stackoverflow$")
    public void theUserIsOnMainPageOfStackoverflow() throws Throwable {
        TestRunner.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (!TestRunner.driver.getCurrentUrl().equals("http://stackoverflow.com/"))
            TestRunner.driver.get("http://stackoverflow.com/");
        TestRunner.driver.manage().window().maximize();

    }
}
