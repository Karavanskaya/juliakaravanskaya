@testrozetka
Feature: testing rozetka

  Background:
    Given the user is on main page

  Scenario: 001 check logo presents
    Given I on rozetka start page
    Then I see page with rozetka logo

  @two
  Scenario: 002 check Apple field presents
    Given I on rozetka start page
    Then I see page with Apple field

  @two
  Scenario: 003 check Telephony, mp3 field presents
    Given I on rozetka start page
    Then I see page with Telephony,mp3 field

  Scenario: 004 check cities present
    Given I on rozetka start page
    When I click link City
    Then I see page with Odessa, Kharkov, Kiev cities

  Scenario: 005 check kart is empty
    Given I on rozetka start page
    When I click link Kart
    Then I see page with kart is empty