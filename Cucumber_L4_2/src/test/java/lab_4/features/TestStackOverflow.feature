@teststackoverflow
Feature: testing stackoverflow
  Background:
    Given the user is on main page of Stackoverflow

  Scenario: 001 check featured count is more then 300
    Given I on stackoverflow start page
    Then I see page with featured count more then 300

  @two
  Scenario: 002 check Google and Facebook present in Sign up page
    Given I on stackoverflow start page
    When I click link Sign up button
    Then I see page with Google and Facebook button

  @two
  Scenario: 003 check date on any Top Question
    Given I on stackoverflow start page
    When I click on any Top Question
    Then I see page with today date