package lab_4.myMapping;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by ASUS on 12.06.2016.
 */
public class RozetkaMainPage {

    private WebDriver driver;

    public RozetkaMainPage(WebDriver driver)
    {
        PageFactory.initElements(driver,this);
        this.driver = driver;

    }


    @FindBy(xpath = ".//img[contains(@alt, 'Интернет магазин Rozetka.ua™ - №1')]")
    public WebElement logo;

    @FindBy(xpath = "//ul/li/a[contains(@href,'apple')]")
    public WebElement apple_in_catalog;

    @FindBy(xpath = "//ul/li/a[contains(@href,'mp3')]")
    public WebElement mp3_in_catalog;

    @FindBy(xpath = ".//*[@id='city-chooser']/a")
    public WebElement city_chooser;

    @FindBy(xpath = ".//div[@class='header-city-box clearfix']//a[contains(text(),'Одесса')]")
    public WebElement Odessa;

    @FindBy(xpath = ".//div[@class='header-city-box clearfix']//a[contains(text(),'Киев')]")
    public WebElement Kiev;

    @FindBy(xpath = ".//*[@id='city-chooser']/descendant::*[@locality_id='31']")
    public WebElement Kharkov;

    @FindBy(xpath = ".//div/*/a[contains(@href,'cart')]")
    public WebElement cart_link;

    @FindBy(xpath = ".//div/h2[text()='Корзина пуста']")
    public WebElement cart_empty;


    public void openCityChooser()
    {

        city_chooser.click();
    }

    public void openCart()
    {
        cart_link.click();
    }

}
