package lab_4.myMapping;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by ASUS on 12.06.2016.
 */
public class StackoverflowMainPage {

    private WebDriver driver;


    public StackoverflowMainPage(WebDriver driver)
    {
        PageFactory.initElements(driver,this);
        this.driver = driver;

    }

    @FindBy(xpath=".//div[@id='tabs']/*/span[contains(@class,'bounty-indicator-tab')]")
    public static WebElement featured;

    @FindBy(xpath=".//*[@id='tell-me-more']")
    public static WebElement signUp;

    @FindBy(xpath=".//div[@class='summary']/*/a[contains(text(),'How')]")
    public static WebElement topQuestion;

    public static int getFeaturedNumber()
    {

        return Integer.parseInt(featured.getText());

    }


    public StackoverflowSignUpPage goToSignUp() throws InterruptedException
    {
        signUp.click();
        TimeUnit.SECONDS.sleep(3);
        return new StackoverflowSignUpPage(driver);

    }


    public StackoverflowTopQuestionPage goToTopQuestion() throws InterruptedException
    {

        topQuestion.click();
        TimeUnit.SECONDS.sleep(3);
        return new StackoverflowTopQuestionPage(driver);

    }
}
