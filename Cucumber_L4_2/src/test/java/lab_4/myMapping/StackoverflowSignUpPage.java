package lab_4.myMapping;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by ASUS on 12.06.2016.
 */
public class StackoverflowSignUpPage {

    private WebDriver driver;

    public StackoverflowSignUpPage(WebDriver driver)
    {
        PageFactory.initElements(driver,this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//div/*/span[contains(text(),'Google')]")
    public WebElement google;

    @FindBy(xpath = ".//div/*/span[contains(text(),'Facebook')]")
    public WebElement facebook;

}
