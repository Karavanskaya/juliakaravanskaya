package lab_4.myMapping;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by ASUS on 12.06.2016.
 */
public class StackoverflowTopQuestionPage {

    private WebDriver driver;


    public StackoverflowTopQuestionPage(WebDriver driver)
    {
        PageFactory.initElements(driver,this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[@id='qinfo']//tbody/tr/td/p/b[contains(text(),'today')]")
    public WebElement asked;

    public String getAskedText()
    {

        return asked.getText();

    }
}
