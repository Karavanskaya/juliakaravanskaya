package lab_4.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lab_4.run.TestRunner;
import org.openqa.selenium.By;
import lab_4.myMapping.RozetkaMainPage;
import java.util.concurrent.TimeUnit;
import static lab_4.run.TestRunner.driver;
import static org.junit.Assert.*;
/**
 * Created by ASUS on 08.06.2016.
 */
public class MyStepdefsRoz {

    public RozetkaMainPage rozetkaMainPage;

    @Given("^I on rozetka start page$")
    public void iOnRozetkaStartPage() throws Throwable {

        assertTrue("Search form is not displayed",
                TestRunner.driver.findElement(By.xpath(".//*[@id='search']/form")).isDisplayed());

    }

    @Then("^I see page with rozetka logo$")
    public void iSeePageWithRozetkaLogo() throws Throwable {
        assertTrue("Log is not displayed",
                rozetkaMainPage.logo.isDisplayed());
    }

    @Then("^I see page with Apple field$")
    public void iSeePageWithAppleField() throws Throwable {
        assertTrue("'Apple' is not displayed",
                rozetkaMainPage.apple_in_catalog.isDisplayed());
    }

    @Then("^I see page with Telephony,mp(\\d+) field$")
    public void iSeePageWithTelephonyMpField(int arg0) throws Throwable {
        assertTrue("'mp3' is not displayed",
                rozetkaMainPage.mp3_in_catalog.isDisplayed());

    }

    @When("^I click link City$")
    public void iClickLinkCity() throws Throwable {
        rozetkaMainPage.openCityChooser();

    }

    @Then("^I see page with Odessa, Kharkov, Kiev cities$")
    public void iSeePageWithOdessaKharkovKievCities() throws Throwable {
        assertTrue("'Одесса' is not displayed",
                rozetkaMainPage.Odessa.isDisplayed());
        assertTrue("'Харьков' is not displayed",
                rozetkaMainPage.Kharkov.isDisplayed());
        assertTrue("'Киев' is not displayed",
                rozetkaMainPage.Kiev.isDisplayed());


    }

    @When("^I click link Kart$")
    public void iClickLinkKart() throws Throwable {
        rozetkaMainPage.openCart();

    }

    @Then("^I see page with kart is empty$")
    public void iSeePageWithKartIsEmpty() throws Throwable {
        assertTrue("'Корзина пуста' is not displayed",
                rozetkaMainPage.cart_empty.isDisplayed());

    }

    @Given("^the user is on main page$")
    public void theUserIsOnMainPage() throws Throwable
    {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //driver.manage().window().maximize();
        if (!driver.getCurrentUrl().equals("http://rozetka.com.ua/"))
            driver.get("http://rozetka.com.ua/");
        rozetkaMainPage = new RozetkaMainPage(driver);
    }

}
