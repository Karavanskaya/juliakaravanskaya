package lab_4.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lab_4.run.TestRunner;
import lab_4.myMapping.StackoverflowMainPage;
import lab_4.myMapping.StackoverflowSignUpPage;
import lab_4.myMapping.StackoverflowTopQuestionPage;
import org.openqa.selenium.By;
import static lab_4.run.TestRunner.driver;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created by ASUS on 09.06.2016.
 */
public class MyStepdefsStack {

    public  StackoverflowMainPage mainPage;
    public  StackoverflowSignUpPage signUpPage;
    public  StackoverflowTopQuestionPage topQuestionPage;

    @Given("^I on stackoverflow start page$")
    public void iOnStackoverflowStartPage() throws Throwable {
        assertTrue("Log is not displayed",
                TestRunner.driver.findElement(By.xpath(".//*[@id='hlogo']/a")).isDisplayed());
    }

    @Then("^I see page with featured count more then (\\d+)$")
    public void iSeePageWithFeaturedCountMoreThen(int arg) throws Throwable {
        assertTrue("Количество в табе ‘featured' меньше 300",
                mainPage.getFeaturedNumber() > arg);

    }

    @When("^I click link Sign up button$")
    public void iClickLinkSignUpButton() throws Throwable {
        signUpPage = mainPage.goToSignUp();

    }

    @Then("^I see page with Google and Facebook button$")
    public void iSeePageWithGoogleAndFacebookButton() throws Throwable {
        assertTrue("'Google' is not displayed",
                signUpPage.google.isDisplayed());
        assertTrue("'Facebook' is not displayed",
                signUpPage.facebook.isDisplayed());

    }

    @When("^I click on any Top Question$")
    public void iClickOnAnyTopQuestion() throws Throwable {
        mainPage = new StackoverflowMainPage(driver);
        topQuestionPage = mainPage.goToTopQuestion();

    }

    @Then("^I see page with today date$")
    public void iSeePageWithTodayDate() throws Throwable {
        assertTrue("Can't find element",
                topQuestionPage.asked.isDisplayed());

    }

    @Given("^the user is on main page of Stackoverflow$")
    public void theUserIsOnMainPageOfStackoverflow() throws Throwable {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (!driver.getCurrentUrl().equals("http://stackoverflow.com/"))
            driver.get("http://stackoverflow.com/");
        driver.manage().window().maximize();
        mainPage = new StackoverflowMainPage(driver);
    }

    }

