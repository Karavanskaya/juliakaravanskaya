package Second;

/**
 * Created by tsybizova on 4/7/2016.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Operations {
    public static void main(String[] args) throws IOException {
        // Task 1
        System.out.println("Тут будет выполнено первое задание:");
        System.out.println("Рассмотрим результат арифметических операций:");
     int a = 4;
     int b = 2;
     int c = 5;
     int d = 3;
        System.out.println(a + b);
        System.out.println(a - b);
        System.out.println(a / b);
        System.out.println(a * b);
        System.out.println(c % d + "\n");

        System.out.println("Рассмотрим результат операций сравнения:");
      int e = 10;
      int f = 20;
        System.out.println("e>f = " + (e > f));
        System.out.println("e<f = " + (e < f));
        System.out.println("e==f = " + (e == f));
        System.out.println("e>=f = " + (e >= f));
        System.out.println("e<=f = " + (e <= f));
        System.out.println("e!=f = " + (e != f)+ "\n");

        System.out.println("Рассмотрим результат логических операций:");
        boolean g = true;
        boolean h = false;
        boolean j = true;
        System.out.println("!g = " + (!g));
        System.out.println("!h = " + (!h));
        System.out.println("h|g =" + (h|g));
        System.out.println("g|j =" + (g|j));
        System.out.println("h&g =" + (h&g));
        System.out.println("g&j =" + (g&j));
        System.out.println("g^h =" + (g^h));
        System.out.println("g^j =" + (g^j)+ "\n");

        System.out.println("Рассмотрим результат операций присвоения:");
        a+=b;
        c-=d;
        f/=e;
        System.out.println(a);
        System.out.println(c);
        System.out.println(f + "\n" + "\n");



        // Task 2
        System.out.println("Тут будет выполнено второе задание:");
        System.out.println("Рассмотрим как происходит преобразование примитивных типов в строку и наоборот:");

        int intk = 3;
        String intstr = Integer.toString(intk);
        System.out.println(intstr);

        int intk2 = Integer.valueOf(intstr);
        System.out.println(intk2);

        double  doublek = 3.2;
        String doublestr = Double.toString(doublek);
        System.out.println(doublestr);

        double doublek2 = Double.valueOf(doublestr);
        System.out.println(doublek2);

        long  longk = 2222222L;
        String longstr = Long.toString(longk);
        System.out.println(longstr);

        long longk2 = Long.valueOf(longstr);
        System.out.println(longk2);

        float  floatk = 3.46F;
        String floatstr = Float.toString(floatk);
        System.out.println(floatstr);

        float floatk2 = Float.valueOf(floatstr);
        System.out.println(floatk2);

        char  chark = 'k';
        String charstr = Character.toString(chark);
        System.out.println(charstr);

        String charstr2 = new String("k");
        System.out.print("Return Value:" );
        System.out.println(charstr2.toCharArray() );

        boolean  boolk = true;
        String boolstr = Boolean.toString(boolk);
        System.out.println(boolstr);

        boolean boolk2 = Boolean.valueOf(boolstr);
        System.out.println(boolk2 + "\n");

        // Task 3
        System.out.println("Тут будет выполнено третье задание:");
        System.out.println("Преобразовываем тип int в float и наоборот:");
        int i = 20;
        float fl = (float) (i);
        System.out.println(fl);

        float f2 = 20.22F;
        int i2 = (int) (f2);
        System.out.println(i2 + "\n");

        // Task 4
        System.out.println("Тут будет выполнено четвертое задание:");
        BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));
        String s1 = "";
        String s2 = "";
        String s3 = "";
        double var1;
        double var2;
        double res = 0;

        System.out.println("Введите первое число:");
        s1 = br.readLine();

        System.out.println("Введите математическую операцию:");
        s2 = br.readLine();

        System.out.println("Введите второе число:");
        s3 = br.readLine();

        var1 = Double.valueOf(s1);
        var2 = Double.valueOf(s3);

        switch (s2){
            case "+": {
                System.out.println("Результат сложения: " + (var1 + var2));
                break;}

            case "-": {
                System.out.println("Результат вычитания: " + (var1 - var2));
                break;}

            case "*": {
                System.out.println("Результат умножения: " + (var1 * var2));
                break;}

            case "/": {
                System.out.println("Результат деления: " + (var1 / var2));
                break;}
            default: {
                System.out.println("Вы ввели не математическую операцию, введите корректный знак операции");
            }
            }

        }






    }

