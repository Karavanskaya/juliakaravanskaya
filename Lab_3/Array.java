package Third;

import java.io.IOException;
import java.util.Random;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.Math;

/**
 * Created by tsybizova on 4/8/2016.
 */


public class Array {
    public static void main(String[] args) throws IOException {
        // Task 1

        System.out.println("Тут будет выполнено первое задание:");
//        Тут закоменчены первые 2 более простых решения данной задачи с двумя типами for
//        System.out.println("Рассмотрим минимальное значение с чисел массива, которые мы задаем вручную:");
//
//        int[] mas = {4, 78, 44, 2};
//        int min = mas[0];
//        for (int i = 0; i < mas.length; i++) {
//            if (min > mas[i]) {
//                min = mas[i];
//            }
//        }
//        System.out.println("Минимальное число:" + min + "\n");

        // System.out.println("Решение с использование второго типа for ");
        // int a[] = {10, 11, 12, 20};
//        int min = a[0];
//        for(int x: a) {
//            if(x < min) min = x;
//        }
//        System.out.println("Minimal element: " + min);
        System.out.println("Тут происходит генерация случайных чисел:");
        int[] a = new int[10];
        Random rand = new Random();
        for (int i = 0; i < a.length; i++)
            a[i] = rand.nextInt(100);
        for (int i = 0; i < a.length; i++)
            System.out.println(+a[i] + " ");
        int min2 = a[0];
        for (int i = 0; i < a.length; i++)
            if (min2 > a[i]) {
                min2 = a[i];
            }
        System.out.println("С этих числел минимальное:" + min2 + "\n");
// Task 2
        System.out.println("Тут будет выполнено второе задание:");
        System.out.println("Выводим нечетные числа от 1 до 99:");


        for (int i = 0; i < 100; i++) {
            if (i % 2 != 0) {
                System.out.print(i + ", ");
            }
        }


// Task 3
        System.out.println("\n" + "\n" + "Тут будет выполнено третье задание:");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String s = "";
        System.out.println("Задайте крайнее число диапазона простых чисел:");
        s = br.readLine();

        System.out.println("Выводим все простые числа от 1 до заданного числа:");

        int ostatok = 1;

        try {
            int k = Integer.valueOf(s);
            for (int i = 2; i <= k; i++) {
                for (int j = 2; j < i; j++)


                {
                    ostatok = i % j;
                    if (ostatok == 0) {
                        break;
                    }

                }
                if (ostatok != 0) {
                    System.out.println(i + " ");
                }
            }

        }
        catch (Exception e)
        {
            System.out.println("Вы ввели не число, попробуйте еще раз - введите именно число!");
        }

// Task 4
        System.out.println("\n" + "\n" + "Тут будет выполнено четвертое задание:");
        System.out.println("Выведим на экран все элементы массива:");

        int[] mas = {4, 78, 44, 2};
        for (int i = 0; i < mas.length; i++) {
            System.out.println(mas[i]);
        }

       }

   }





