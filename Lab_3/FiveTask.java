package Third;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by tsybizova on 4/11/2016.
 */
public class FiveTask {
    public static void main(String[] args) throws IOException {

        System.out.println("\n" + "\n" + "Тут будет выполнено пятое задание:");
        BufferedReader read = new BufferedReader(new InputStreamReader(System.in));

        String s1 = "0";
        double sum = 0;
        System.out.println("Введите числа, которые будут суммироваться, пока вы не введете слово 'Сумма':");

        while (!s1.equals("Сумма")) {

            sum += Double.valueOf(s1);
            s1 = read.readLine();

        }

        if (sum % 1 == 0) {
            System.out.println("Сумма введенных чисел:" + (int)sum);
        }
        else  {
            System.out.println("Сумма введенных чисел:" + sum);

        }
    }
}
