package PackFour;

/**
 * Created by tsybizova on 4/14/2016.
 */

import java.lang.Math;

public abstract class Car {
    double maxV;
    double speedUp;
    double mobility;
    double startV;
    double v;
    double time;
    double accConst;
    public abstract void Turn ();

    public void SpeedAcc ( ){
        int s=2000; double s1= 0;
        v = Math.sqrt(Math.pow(startV, 2)+ (speedUp*2*s));
        if (v>maxV) {

            s1= ((Math.pow(maxV, 2) - Math.pow(startV, 2)) / (2*speedUp));
            time += (maxV - startV) / speedUp;
            time += ((s-s1)/maxV);

        }
        else {
            time += (v - startV) / speedUp;
        }

    }

    public void Convert () {
        v = v * 1000 / 3600;

    }
}
