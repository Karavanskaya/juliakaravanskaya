package PackFour;

/**
 * Created by tsybizova on 4/14/2016.
 */


public class MainClass {
    public static void main(String[] args) throws Exception  {
        Car[] carArr = new Car[3];


        carArr[0] = new Car1(220.00, 1.5, 0.4);
        carArr[1] = new Car2(200.00, 1.2, 0.8);
        carArr[2] = new Car3(240, 1.8, 0.7);

        for (int i = 0; i < 3; i++) {
            carArr[i].Convert();
            for (int j = 0; j < 20; j++) {
                carArr[i].SpeedAcc();
                carArr[i].Turn();
            }
        }



        for(int i=carArr.length-1;i>0;i--){

            for(int j=0;j<i;j++) {

                if (carArr[j].time > carArr[j + 1].time) {

                    double tmp = carArr[j].time;
                    carArr[j].time = carArr[j + 1].time;
                    carArr[j + 1].time = tmp;
                   }
            }
        }

        for (int j = 0; j < 3; j++) {


            System.out.println("Машина пройдет за: " + ((int) (carArr[j].time) / 60) + " минут " + ((int) (carArr[j].time) % 60) + " секунд");
        }
    }
}


