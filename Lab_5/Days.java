import java.util.Calendar;
import java.util.Date;

/**
 * Created by ASUS on 25.04.2016.
 */

public class Days {
    public static int Day(Date d) throws WorkDayExc, WeekendExc {
        Calendar startDate = Calendar.getInstance();
        startDate.setTime(d);
        if (startDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || startDate.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
            new WeekendExc(d);
        } else {
            new WorkDayExc(d);
        }
        return 0;
    }
}

