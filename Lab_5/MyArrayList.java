import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by ASUS on 03.05.2016.
 */
public class MyArrayList implements List{
    private Object[] mas;

    public MyArrayList() {

        mas = new Object[0];
    }

    @Override
    public int size() {

        return mas.length;

    }

    @Override
    public boolean isEmpty() {

        if (mas.length == 0) {

            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean contains(Object o) {
        boolean b = false;
        for (int i = 0; i < mas.length; i++) {
            if (mas[i] == o) {
                b = true;
                break;
            }
        }
        return b;

    }

    @Override
    public Iterator iterator() {

        return new MyIterator();
    }

    public Object[] toArray() {
        Object[] tempMas = new Object[mas.length];
        for (int i = 0; i < mas.length; i++) {
            tempMas[i] = mas[i];
        }

        return tempMas;

    }

    @Override
    public boolean add(Object o) {
        try {
            int n = mas.length + 1;
            Object[] tmpArray = new Object[n];
            for (int i = 0; i < mas.length; i++) {
                tmpArray[i] = mas[i];
            }
            tmpArray[tmpArray.length - 1] = o;
            this.mas = new Object[tmpArray.length];
            this.mas = tmpArray;
            return true;
        } catch (Exception e) {

            return false;
        }
    }

    @Override
    public boolean remove(Object o) {
        int num = -1;
        boolean b = false;

        for (int i = 0; i < mas.length; i++) {
            if (mas[i] == o) {
                num = i;
                b = true;
                Object[] tmpArray = new Object[mas.length - 1];
                for (int j = 0; j < num; j++) {
                    tmpArray[j] = mas[j];
                }
                for (int j = num + 1; j < tmpArray.length; j++) {
                    tmpArray[j - 1] = mas[j];
                }
                this.mas = new Object[tmpArray.length];
                this.mas = tmpArray;
                break;
            }

        }

        return b;
    }

    @Override
    public boolean addAll(Collection c) {
        try {
            int n = mas.length + c.size();
            Object[] tmpArray = new Object[n];
            Object[] tmp1 = c.toArray();
            for (int i = 0; i < mas.length; i++) {
                tmpArray[i] = mas[i];
                tmpArray[mas.length + i] = tmp1[i];
            }

            this.mas = new Object[tmpArray.length];
            this.mas = tmpArray;
            return true;
        } catch (Exception e) {

            return false;
        }

    }

    @Override
    public boolean addAll(int index, Collection c) {
        try {
            int n = mas.length + c.size();
            Object[] tmpArray = new Object[n];
            Object[] tmp1 = c.toArray();
            Object[] tmp2 = new Object[n - index - c.size()];
            for (int i = 0; i < index; i++) {
                tmpArray[i] = mas[i];
            }
            for (int i = 0; i < c.size(); i++) {
                tmpArray[index + i] = tmp1[i];
            }
            for (int i = index + c.size(), j = 0; i < n; i++, j++) {
                    tmpArray[i] = mas[index + j];
            }

            this.mas = new Object[tmpArray.length];
            this.mas = tmpArray;
            return true;
        } catch (Exception e) {

            return false;
        }
    }

    @Override
    public void clear() {

        this.mas = new Object[0];

    }

    @Override
    public Object get(int index) {

        return mas[index];
    }

    @Override
    public Object set(int index, Object element) {

        mas[index] = element;

        return mas[index];
    }

    @Override
    public void add(int index, Object element) {
        int n = mas.length + 1;
        Object[] tmpArray = new Object[n];
        for (int i = 0; i < index; i++) {
            tmpArray[i] = mas[i];
        }
        tmpArray[index] = element;
        for (int i = index; i < n; i++) {
            tmpArray[i + 1] = mas[i];
        }

        this.mas = new Object[tmpArray.length];
        this.mas = tmpArray;


    }


    @Override
    public Object remove(int index) {

        int n = mas.length - 1;
        Object tmp = mas[index];
        Object[] tmpArray = new Object[n];
        for (int i = 0; i < index; i++) {
            tmpArray[i] = mas[i];
        }

        for (int i = index; i < n; i++) {
            tmpArray[i] = mas[i + 1];
        }

        this.mas = new Object[tmpArray.length];
        this.mas = tmpArray;

        return tmp;
    }

    @Override
    public int indexOf(Object o) {
        int num = -1;
        for (int i = 0; i < mas.length; i++) {
            if (mas[i] == o) {
                num = i;
                break;
            }
        }
        return num;
    }

    @Override
    public int lastIndexOf(Object o) {
        int num = -1;
        for (int i = 0; i < mas.length; i++) {
            if (mas[i] == o) {
                num = i;
            }
        }
        return num;

    }

    @Override
    public ListIterator listIterator() {
        return new MyListIterator();
    }

    @Override
    public ListIterator listIterator(int index) {
        return new MyListIterator(index);
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        int n = toIndex - fromIndex;
        MyArrayList tmp = new MyArrayList();
        for (int i = fromIndex; i < toIndex; i++) {
            tmp.add(mas[i]);
        }
        return tmp;
    }

    @Override
    public boolean retainAll(Collection c) {
        try {
            Object[] tmp1 = c.toArray();
            Object[] flags = new Object[mas.length];
            boolean flag = false;
            for (int i = 0; i < mas.length; i++) {
                flag = false;
                for (int j = 0; j < c.size(); j++) {
                    if (mas[i] == tmp1[j]) {
                        flag = true;
                    }

                }
                if (flag == false) {
                    flags[i] = mas[i];
                } else {

                    flags[i] = null;
                }
            }
            for (int i = 0; i < flags.length; i++) {
                if (flags[i] != null) {
                    this.remove(flags[i]);
                }
            }

            return true;

        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean removeAll(Collection c) {
        try {
            Object[] tmp1 = c.toArray();
            Object[] flags = new Object[mas.length];
            boolean flag = false;
            for (int i = 0; i < mas.length; i++) {
                flag = false;
                for (int j = 0; j < c.size(); j++) {
                    if (mas[i] == tmp1[j]) {
                        flag = true;

                    }

                }
                if (flag == true) {
                    flags[i] = mas[i];
                } else {

                    flags[i] = null;
                }
            }
            for (int i = 0; i < flags.length; i++) {
                if (flags[i] != null) {
                    this.remove(flags[i]);
                }
            }

            return true;

        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean containsAll(Collection c) {
        Object[] tmp1 = c.toArray();
        int counter = 0;
        for (int i = 0; i < tmp1.length; i++) {
            for (int j = 0; j < mas.length; j++) {
                if (tmp1[i] == mas[j]) {
                    counter += 1;
                    break;
                }
            }
        }
        if (counter == tmp1.length) {
            return true;
        } else {
            return false;
        }


    }

    @Override
    public Object[] toArray(Object[] a) {


        if (a.length < mas.length) {
            Object[] array = new Object[mas.length];
            for (int i = 0; i < mas.length; i++) {
                array[i] = mas[i];
            }
            return array;
        } else {
            for (int i = 0; i < mas.length; i++) {
                a[i] = mas[i];
            }
            for (int i = mas.length; i < a.length; i++) {
                a[i] = null;

            }
            return a;
        }


    }


    private class MyIterator implements Iterator {

        private int ind;


        @Override
        public boolean hasNext() {

            return ind < mas.length;
        }

        @Override
        public Object next() {
            return mas[ind++];
        }
    }

    private class MyListIterator implements ListIterator {

        private int ind;
        private boolean flag;

        public MyListIterator() {
            ind = 0;
        }

        public MyListIterator(int i) {
            ind = i;
        }


        @Override
        public boolean hasNext() {
            return ind < mas.length;
        }

        @Override
        public Object next() {
            flag = true;
            return mas[ind++];
        }

        @Override
        public boolean hasPrevious() {
            return ind >= 0;
        }

        @Override
        public Object previous() {
            flag = true;
            return mas[ind--];
        }

        @Override
        public int nextIndex() {
            return ind++;
        }

        @Override
        public int previousIndex() {
            return ind--;
        }

        @Override
        public void remove() {

            if (flag == true) {

                int n = mas.length - 1;
                Object[] tmpArray = new Object[n];
                for (int i = 0; i < ind; i++) {
                    tmpArray[i] = mas[i];
                }

                for (int i = ind; i < n; i++) {
                    tmpArray[i] = mas[i + 1];
                }

                mas = new Object[tmpArray.length];
                mas = tmpArray;

                flag = false;

            }
        }

        @Override
        public void set(Object o) {

            mas[ind] = o;


        }


        @Override
        public void add(Object o) {

            flag = false;
            int n = mas.length + 1;
            Object[] tmpArray = new Object[n];
            for (int i = 0; i < ind+1; i++) {
                tmpArray[i] = mas[i];
            }
            tmpArray[ind+1] = o;
            for (int i = ind+1; i < n; i++) {
                tmpArray[i + 1] = mas[i];
            }

            mas = new Object[tmpArray.length];
            mas = tmpArray;


        }
    }
}

