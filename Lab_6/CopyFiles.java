import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Created by tsybizova on 5/6/2016.
 */
public class CopyFiles extends Thread{
    private String str;
    private int pointer;


    public void Copying(String str, int i){

        this.str = str;
        this.pointer = i;
    }
    @Override
    public void run(){
        Path source = Paths.get("D:\\NewFolder");
        try {

            Files.walkFileTree(source, new MyFileVisitor());


        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    public class MyFileVisitor extends SimpleFileVisitor {




        @Override
        public FileVisitResult visitFile(Object file, BasicFileAttributes attrs) throws IOException {

            Path source = Paths.get("D:\\NewFolder");
            Path pat = (Path) file;
            File fil = pat.toFile();
            String tmpS = source.toString();
            tmpS+=Integer.toString(pointer);

            Path target = Paths.get(tmpS);

            if(fil.getName().contains(str)){

                if(Files.notExists(target)){
                    Files.createDirectories(target);
                }
                target= target.resolve(fil.getName());
                Files.copy(pat, target, StandardCopyOption.REPLACE_EXISTING);

            }
            return FileVisitResult.CONTINUE;

        }
    }
}
