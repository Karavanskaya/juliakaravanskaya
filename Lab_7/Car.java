
import java.io.Serializable;

/**
 * Created by tsybizova on 5/10/2016.
 */
public class Car implements Serializable {

    double acceler;
    double speed;
    double time;
    int k;



    public Car(double a, int ras){

        acceler = a;
        k = ras;
        time = 0;
        speed = 0;

    }


    public boolean Move() {

        if(k >=0 && acceler >=0) {
            speed = Math.round(Math.sqrt(2 * k * acceler));

            time = Math.round(time + speed / acceler);
            return true;
        }
        else {
            return false;
        }
    }

}
