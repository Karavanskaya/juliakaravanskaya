import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by tsybizova on 5/10/2016.
 */
public class MainClass {
    public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Car car1  = new Car(3.2, 2500);
        CreateDirFile();
        MySerialize(car1);
        car1 = MyDeserialize();
        Class c1 = car1.getClass();
        Method m1 = c1.getMethod("Move");
        m1.invoke((Car)car1);
        System.out.println(car1.acceler +" "+car1.k + " "+car1.time+" "+car1.speed);




    }

    public static Car MyDeserialize() throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream("D:\\Lab_7/Serialize.ser");
        ObjectInputStream ois = new ObjectInputStream(fis);
        Car car = (Car) ois.readObject();
        ois.close();
        fis.close();
        return car;
    }

    public static void MySerialize(Car car) throws IOException {
        FileOutputStream fos = new FileOutputStream("D:\\Lab_7/Serialize.ser");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(car);
        oos.close();
        fos.close();
    }

    private static void CreateDirFile() throws IOException {
        File file = new File("D:\\Lab_7");
        file.mkdirs();
        File file1 = new File("D:\\Lab_7/Serialize.ser");
        file1.createNewFile();
    }
}
