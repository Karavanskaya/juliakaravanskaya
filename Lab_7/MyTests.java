import org.junit.Assert;
import org.junit.Test;


import java.io.*;

/**
 * Created by tsybizova on 5/10/2016.
 */
public class MyTests {
    @Test
    public void TestConstructor() {
        double acc = 3;
        int s = 2500;
        Car car1 = new Car(acc, s);
        Assert.assertTrue(car1.acceler ==3 && car1.k == 2500);
    }

    @Test
    public void TestMethodMoveValidData(){
        Car car1 = new Car(3, 3000);
        car1.Move();
        Assert.assertTrue(car1.speed ==Math.round(Math.sqrt(2*3*3000)));
    }

    @Test

    public void TestMethodMoveInvalidData(){
        Car car1 = new Car(-1,-1);
        Assert.assertFalse(car1.Move());

    }

    @Test
    public void TestDirFileCreation(){
        File file = new File("D:\\Lab_7/Serialize.ser");
        Assert.assertTrue(file.exists());

    }

    @Test
    public void TestSerializeDeserialize() throws IOException, ClassNotFoundException {
        double acc1 = 5.5;
        int k1 = 2;
        Car car2  = new Car(acc1, k1);
        MainClass.MySerialize(car2);
        car2.acceler = 0;
        car2.k = 0;
        car2 = MainClass.MyDeserialize();
        Assert.assertTrue(car2.acceler == acc1);
        Assert.assertTrue(car2.k == k1);



    }
}
