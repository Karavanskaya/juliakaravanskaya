package PageMapping;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.assertTrue;

/**
 * Created by ASUS on 06.06.2016.
 */
public class RozetkaMainPage {

    private WebDriver driver;

    public RozetkaMainPage(WebDriver driver){
        PageFactory.initElements(driver,this);
        this.driver = driver;
    }


    @FindBy(xpath=".//img[contains(@alt, 'Интернет магазин Rozetka.ua™ - №1')]")
    public static WebElement logo;

    @FindBy(xpath = "//ul/li/a[contains(@href,'apple')]")
    public static WebElement apple_link;

    @FindBy(xpath = "//ul/li/a[contains(@href,'mp3')]")
    public static WebElement mp3_link;

    @FindBy(xpath = ".//*[@id='city-chooser']/a/span")
    public static WebElement city_chooser;

    @FindBy(xpath = ".//*[@id='city-chooser']/descendant::*[@locality_id='1']")
    public static WebElement city_Kiev;

    @FindBy(xpath = ".//*[@id='city-chooser']/descendant::*[@locality_id='30']")
    public static WebElement city_Odessa;

    @FindBy(xpath = ".//*[@id='city-chooser']/descendant::*[@locality_id='31']")
    public static WebElement city_Kharkov;

    @FindBy(xpath = ".//div/*/a[contains(@href,'cart')]")
    public static WebElement korzina_link;

    @FindBy(xpath = ".//div/h2[text()='Корзина пуста']")
    public static WebElement korzina_pysta;

    public static void openCityChooser()
    {
        city_chooser.click();
    }

    public static void openKorzina()
    {
        korzina_link.click();
    }

}

