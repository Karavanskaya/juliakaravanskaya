package PageMapping;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
/**
 * Created by ASUS on 06.06.2016.
 */
public class StackoverflowSignUpPage {
    private WebDriver driver;

    public StackoverflowSignUpPage(WebDriver driver)
    {
        PageFactory.initElements(driver,this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//div/*/span[contains(text(),'Google')]")
    public static WebElement google;

    @FindBy(xpath = ".//div/*/span[contains(text(),'Facebook')]")
    public static WebElement facebook;
}
