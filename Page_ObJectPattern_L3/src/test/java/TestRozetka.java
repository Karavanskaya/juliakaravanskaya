import PageMapping.RozetkaMainPage;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.By;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;



/**
 * Created by ASUS on 06.06.2016.
 */
public class TestRozetka {
    private static WebDriver driver;
    private static RozetkaMainPage rozetkaMainPage;
    private static String baseUrl;

    @BeforeClass
    public static void SetUp() throws Exception{
        driver = new FirefoxDriver();
        baseUrl = "http://rozetka.com.ua/";
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        rozetkaMainPage = new RozetkaMainPage(driver);
    }

    @Before
    public void setBeforeEachTest() throws Exception {
        driver.get(baseUrl);
        driver.manage().window().maximize();

    }

    @AfterClass
    public static void tearDown() throws Exception{

        driver.close();
    }

    @Test
    public void TestRozLogo() {
        assertTrue("Логотип 'Розетка' не отображается", RozetkaMainPage.logo.isDisplayed());
    }


    @Test
    public void TestRozAppleLink() throws InterruptedException{
        assertTrue("Поле 'Apple' не отображается", RozetkaMainPage.apple_link.isDisplayed());
    }

    @Test
    public void TestRozMp3Link()throws InterruptedException {
        assertTrue("Поле 'Apple' не отображается", RozetkaMainPage.mp3_link.isDisplayed());
    }

    @Test
    public void TestRozCities() {
//
//        WebElement gorod = null;
//        try {
//            gorod = driver.findElement(By.xpath(".//*[@id='city-chooser']/a"));
//        } catch (Exception ex) {
//        }
//        assertNotNull("Can't find link", gorod);
//
//        gorod.click();
        rozetkaMainPage.openCityChooser();
        assertTrue("'Киев' не отображается", RozetkaMainPage.city_Kiev.isDisplayed());
        assertTrue("'Харьков' не отображается", RozetkaMainPage.city_Kharkov.isDisplayed());
        assertTrue("'Одесса' не отображается", RozetkaMainPage.city_Odessa.isDisplayed());

    }

    @Test
    public void TestRozKorzina() {
//        WebElement korzina = null;
//        try {
//            korzina = driver.findElement(By.xpath(".//div/*/a[contains(@href,'cart')]"));
//        } catch (Exception ex) {
//        }
//        assertNotNull("'Корзина' не отображается", korzina);
//
//        korzina.click();

      rozetkaMainPage.openKorzina();
        assertTrue("Корзина не пуста", RozetkaMainPage.korzina_pysta.isDisplayed());

    }
}
