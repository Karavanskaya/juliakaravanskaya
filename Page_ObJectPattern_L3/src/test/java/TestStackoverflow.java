import PageMapping.RozetkaMainPage;
import PageMapping.StackoverflowMainPage;
import PageMapping.StackoverflowSignUpPage;
import PageMapping.StackoverflowTopQuestionPage;
import PageMapping.StackoverflowTopQuestionPage;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

/**
 * Created by ASUS on 06.06.2016.
 */
public class TestStackoverflow {
    private static WebDriver driver;
    private static StackoverflowMainPage stackoverflowMainPage;
    private static StackoverflowTopQuestionPage stackoverflowTopQuestionPage;
    private static StackoverflowSignUpPage stackoverflowSignUpPage;
    private static String baseUrl;

    @BeforeClass
    public static void SetUp() {
        driver = new FirefoxDriver();
        baseUrl = "http://stackoverflow.com/";
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        stackoverflowMainPage = new StackoverflowMainPage(driver);
    }

    @Before
    public void setBeforeEachTest() throws Exception {
        driver.get(baseUrl);
        driver.manage().window().maximize();

    }

    @AfterClass
    public static void tearDown() {

        driver.close();
    }

    @Test
    public void TestStackCount() {
        assertTrue("Количество в табе ‘featured' меньше 300", StackoverflowMainPage.getFeaturedNumber()>300);
    }

    @Test
    public void TestStackSignUp() throws InterruptedException{

//        WebElement signUp = null;
//        try {
//            signUp = driver.findElement(By.xpath(".//*[@id='tell-me-more']"));
//        } catch(Exception ex) {}
//        assertNotNull("Can't find link", signUp);
//        signUp.click();

        stackoverflowSignUpPage = stackoverflowMainPage.goToSignUp();
        assertTrue("'Google' не отображается", StackoverflowSignUpPage.google.isDisplayed());
        assertTrue("'Facebook' не отображается", StackoverflowSignUpPage.facebook.isDisplayed());
    }

    @Test
    public void TestStackToday ()throws InterruptedException{
//        WebElement topQuestion = driver.findElement(By.xpath(".//div[@class='summary']/*/a[contains(text(),'How')]"));
//        assertNotNull("Can't find link", topQuestion);
//
//        topQuestion.click();

        stackoverflowTopQuestionPage = stackoverflowMainPage.goToTopQuestion();
        assertTrue("'today' не отображается", StackoverflowTopQuestionPage.asked.isDisplayed());

    }
}
