
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;
import static org.junit.Assert.*;
/**
 * Created by tsybizova on 5/27/2016.
 */
public class TestRozetka {
    private WebDriver driver;
    private String baseUrl;

    @Before
    public void setUp() throws Exception {
        driver = new FirefoxDriver();
        baseUrl = "http://rozetka.com.ua/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    }


    @Test
    public void TestRoz()throws InterruptedException {
        driver.get(baseUrl);
        driver.manage().window().maximize();
        Assert.assertTrue("Логотип 'Розетка' не отображается",
                driver.findElement(By.xpath(".//img[contains(@alt, 'Интернет магазин Rozetka.ua™ - №1')]")).isDisplayed());

        Assert.assertTrue("Поле 'Apple' не отображается",
                driver.findElement(By.xpath("//ul/li/a[contains(@href,'apple')]")).isDisplayed());

        Assert.assertTrue("Секция 'Телефоны, МР3' не отображается",
                driver.findElement(By.xpath("//ul/li/a[contains(@href,'mp3')]")).isDisplayed());

       //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebElement gorod = driver.findElement(By.xpath(".//*[@id='city-chooser']/a"));
        assertNotNull("Can't find link", gorod);

        gorod.click();
        assertTrue("Can't find element",
                driver.findElement(By.xpath(".//div[@class='header-city-box clearfix']//a[contains(text(),'Киев')]")).isDisplayed());

        assertTrue("Can't find element",
                driver.findElement(By.xpath(".//div[@class='header-city-box clearfix']//a[contains(text(),'Одесса')]")).isDisplayed());

        assertTrue("Can't find element",
                driver.findElement(By.xpath(".//*[@id='city-chooser']/descendant::*[@locality_id='31']")).isDisplayed());

        WebElement korzina = driver.findElement(By.xpath(".//div/*/a[contains(@href,'cart')]"));
        assertNotNull("Can't find link", korzina);

        korzina.click();
        assertTrue("Can't find element",
                driver.findElement(By.xpath(".//div/h2[text()='Корзина пуста']")).isDisplayed());

    }

    @After
    public void tearDown()throws Exception {
        driver.quit();
    }
}
