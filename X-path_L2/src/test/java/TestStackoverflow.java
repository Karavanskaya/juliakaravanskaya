import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import static org.junit.Assert.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by ASUS on 31.05.2016.
 */
public class TestStackoverflow {
    private static WebDriver driver;
    public static String baseUrl;

    @BeforeClass
    public static void setUp() throws Exception {
        driver = new FirefoxDriver();
        baseUrl = "http://stackoverflow.com/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    }

    @Test
    public void TestStack() throws InterruptedException {
        driver.get(baseUrl);
        driver.manage().window().maximize();

        Assert.assertTrue("Количество в табе ‘featured' меньше 300",
                Integer.parseInt(driver.findElement(By.xpath(".//div[@id='tabs']/*/span[contains(@class,'bounty-indicator-tab')]")).getText())>300);


        WebElement signUp = driver.findElement(By.xpath(".//*[@id='tell-me-more']"));
        assertNotNull("Can't find link", signUp);

        signUp.click();
        assertTrue("Can't find element",
                driver.findElement(By.xpath(".//div/*/span[contains(text(),'Google')]")).isDisplayed());

        assertTrue("Can't find element",
                driver.findElement(By.xpath(".//div/*/span[contains(text(),'Facebook')]")).isDisplayed());
    }
        @Before
        public void returnToMainPage () {
            driver.get("http://stackoverflow.com/");
        }

        @Test
        public void TestStack2 ()throws InterruptedException {
            WebElement topQuestion = driver.findElement(By.xpath(".//div[@class='summary']/*/a[contains(text(),'How')]"));
            assertNotNull("Can't find link", topQuestion);

            topQuestion.click();

            assertTrue("Can't find element",
                    driver.findElement(By.xpath(".//div/table/tbody/tr/td/p/b[contains(text(),'today')]")).isDisplayed());

        }

        @AfterClass
        public static void tearDown ()throws Exception {
            driver.quit();
        }
    }

